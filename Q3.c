#include <stdio.h>
int main() {
    int row, column, x[100][100], y[100][100], sum[100][100], i, j;
    printf("Enter the number of rows : ");
    scanf("%d", &row);
    printf("Enter the number of columns : ");
    scanf("%d", &column);

    printf("\nEnter elements of 1st matrix:\n");
    for (i = 0; i < row; ++i)
        for (j = 0; j < column; ++j) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &x[i][j]);
        }

    printf("Enter elements of 2nd matrix:\n");
    for (i = 0; i < row; ++i)
        for (j = 0; j < column; ++j) {
            printf("Enter element a%d%d: ", i + 1, j + 1);
            scanf("%d", &y[i][j]);
        }


    for (i = 0; i < row; ++i)
        for (j = 0; j < column; ++j) {
            sum[i][j] = x[i][j] + y[i][j];
        }


    printf("\nSum of two matrices: \n");
    for (i = 0; i < row; ++i)
        for (j = 0; j < column; ++j) {
            printf("%d   ", sum[i][j]);
            if (j == column - 1) {
                printf("\n\n");
            }
        }

    return 0;
}

#include<stdio.h>
#include<string.h>

int main()
{
	char string[100];
	char character;
	int i,length;
	int frequence = 0;

	printf("\nEnter your string: ");
	fgets(string, 100, stdin);
	printf("\nEnter your character : ");
	scanf("%c", &character);
    length = strlen(string);

	for(int i = length ; i >= 0 ; i--)
	{
		if(string[i] == character)
		{
			frequence = frequence + 1;
		}
	}
	printf("\nFrequency of %c is %d.\n\n", character, frequence);
	return 0;
}
